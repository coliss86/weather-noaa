#!/bin/bash -e

SATELLITE_NAME="$1"
FREQUENCY="$2"
WEATHER_TLE="$3"

SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
PREDICTION_START=$(predict -t "${WEATHER_TLE}" -p "${SATELLITE_NAME}" | head -1)
PREDICTION_END=$(predict -t "${WEATHER_TLE}" -p "${SATELLITE_NAME}" | tail -1)

DURATION_BEFORE="60 second ago"
DURATION_AFTER="120" 

NEXT_PREDICTION=$(echo "${PREDICTION_END}" | cut -d " " -f 1)

MAXELEV=$(predict -t "${WEATHER_TLE}" -p "${SATELLITE_NAME}" | awk -v max=0 '{if($5>max){max=$5}}END{print max}')

while [ "$(date --date="TZ=\"UTC\" @${NEXT_PREDICTION}" +%D)" == "$(date +%D)" ]; do

  ORG_START_TIME=$(echo "${PREDICTION_START}" | cut -d " " -f 3-4)
  START_TIME=$(date --date="${ORG_START_TIME} ${DURATION_BEFORE}" +"%d%b%y %H:%M:%S")
  EPOCH_START_TIME=$(echo "${PREDICTION_START}" | cut -d " " -f 1)

  SECOND_START_TIME=$(echo "${START_TIME}" | cut -d " " -f 2 | cut -d ":" -f 3 | sed "s/^0//")

  TIMER=$(( "${NEXT_PREDICTION}" - "${EPOCH_START_TIME}" + "${SECOND_START_TIME}" + "${DURATION_AFTER}" ))

  if [ "${MAXELEV}" -gt 30 ]
  then
    START_HOUR=$(date --date="TZ=\"UTC\" ${START_TIME}" +%H:%M)
    echo "${START_HOUR} ${SATELLITE_NAME//" "} ${MAXELEV}"

    echo "${SCRIPTPATH}/receive_and_process_satellite.sh \"${SATELLITE_NAME}\" ${FREQUENCY} ${WEATHER_TLE} ${EPOCH_START_TIME} ${TIMER} ${MAXELEV} >/dev/null 2>&1" | at "$(date --date="TZ=\"UTC\" ${START_TIME}" +"%H:%M %D")" > /dev/null 2>&1
  fi

  NEXT_PREDICT=$(( "${NEXT_PREDICTION}" + 600 ))

  PREDICTION_START=$(predict -t "${WEATHER_TLE}" -p "${SATELLITE_NAME}" "${NEXT_PREDICT}" | head -1)
  PREDICTION_END=$(predict -t "${WEATHER_TLE}" -p "${SATELLITE_NAME}"  "${NEXT_PREDICT}" | tail -1)

  MAXELEV=$(predict -t "${WEATHER_TLE}" -p "${SATELLITE_NAME}" "${NEXT_PREDICT}" | awk -v max=0 '{if($5>max){max=$5}}END{print max}')

  NEXT_PREDICTION=$(echo "${PREDICTION_END}" | cut -d " " -f 1)
 
done

exit 0
