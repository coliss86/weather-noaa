# Raspberry Pi NOAA Weather Satellite Receiver

Heavily inspired from https://www.instructables.com/id/Raspberry-Pi-NOAA-Weather-Satellite-Receiver/
All scripts have been rewritten in order to be more readable and more versatile.

```
./receive_and_process_satellite.sh "NOAA 18" 137.9125 /home/pi/weather/predict/weather.tle 1591130159 1002 34
```
