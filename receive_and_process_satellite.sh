#!/bin/bash -e

PATH=$PATH:/usr/local/bin/
WEATHER_DIR="/home/pi/weather"

SATELLITE_NAME="$1"
FREQUENCY="$2"
TLE_FILE="$3"
EPOCH_START_TIME="$4"
TIMER="$5"
ELEVATION="$6"

cat << EOT
SATELLITE_NAME=${SATELLITE_NAME}
FREQUENCY=${FREQUENCY}
TLE_FILE=${TLE_FILE}
EPOCH_START_TIME=${EPOCH_START_TIME}
TIMER=${TIMER}
ELEVATION=${ELEVATION}
EOT

CAPTURE_START_TIME=$(date --date="TZ=\"UTC\" @${EPOCH_START_TIME}" +%Y-%m-%d_%H-%M)
FILENAME_BASE="${CAPTURE_START_TIME}_${SATELLITE_NAME//" "}_${ELEVATION}"
WORK_DIR="${WEATHER_DIR}/${FILENAME_BASE}"

mkdir -p "${WORK_DIR}"
cd "${WORK_DIR}"

timeout "${TIMER}" rtl_fm -f "${FREQUENCY}M" -s 60k -g 45 -p 55 -E wav -E deemp -F 9 - | sox -t wav - "${FILENAME_BASE}.wav" rate 11025

PASS_START=$(( EPOCH_START_TIME + 90 ))

if [ -f "${FILENAME_BASE}.wav" ];
then
  MAP_FILE="${FILENAME_BASE}-map.png"
  wxmap -T "${SATELLITE_NAME}" -H "${TLE_FILE}" -p 0 -l 0 -o "${PASS_START}" "${MAP_FILE}"

  # normal infrared
  wxtoimg -m "${MAP_FILE}" -e ZA "${FILENAME_BASE}.wav" "${FILENAME_BASE}.png"
  # color infrared
  wxtoimg -m "${MAP_FILE}" -e NO "${FILENAME_BASE}.wav" "${FILENAME_BASE}-NO.png"
  # map color infrared
  wxtoimg -m "${MAP_FILE}" -e MCIR "${FILENAME_BASE}.wav" "${FILENAME_BASE}-MCIR.png"
  # thermal
  wxtoimg -m "${MAP_FILE}" -e THERM "${FILENAME_BASE}.wav" "${FILENAME_BASE}-THERM.png"

  cp "${TLE_FILE}" .

  rm -f "${MAP_FILE}"

  scp -r "${WORK_DIR}"  jumper@popeye:/mnt/media/sdr/
  scp "${TLE_FILE}"  jumper@popeye:/mnt/media/sdr/tle/
fi

exit 0
