#!/bin/bash -e

SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
WEATHER_DIR="/home/pi/weather"
WEATHER_TLE="${WEATHER_DIR}/tle/$(date +"%Y-%m-%d").tle"
WEATHER_SCHEDULE="${WEATHER_DIR}/weather.txt"
TRACE="${WEATHER_DIR}/passes/$(date +"%Y-%m-%d").txt"

rm -f "${TRACE}"

# Update Satellite Information
wget -qr https://www.celestrak.com/NORAD/elements/weather.txt -O "${WEATHER_SCHEDULE}"

mkdir -p $(dirname "${WEATHER_TLE}") $(dirname "${TRACE}")

rm -f "${WEATHER_TLE}"
grep "NOAA 18" "${WEATHER_SCHEDULE}" -A 2 >> "${WEATHER_TLE}"
grep "NOAA 19" "${WEATHER_SCHEDULE}" -A 2 >> "${WEATHER_TLE}"

rm -f "${WEATHER_SCHEDULE}"

#Remove all AT jobs
for i in $(atq | awk '{print $1}'); do atrm "$i"; done

#Schedule Satellite Passes:
"${SCRIPTPATH}/schedule_satellite.sh" "NOAA 19" 137.1000 "${WEATHER_TLE}" >> "${TRACE}"
"${SCRIPTPATH}/schedule_satellite.sh" "NOAA 18" 137.9125 "${WEATHER_TLE}" >> "${TRACE}"

rm -f "${TRACE}.new"
sort "${TRACE}" | uniq > "${TRACE}.new"
mv "${TRACE}.new" "${TRACE}"

echo "Schedule : "
cat "${TRACE}"

exit 0
